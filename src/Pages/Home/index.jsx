import React from 'react';
import { ContainerHome} from './style.js';
import SearchPage from '../../Components/SearchPage'

export default function Home () {

    return (
        <ContainerHome>
            <SearchPage/>
        </ContainerHome>
       
    )

}