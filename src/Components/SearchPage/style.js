import styled from "styled-components";

export const ContainerSeachPage = styled.div`
    margin-top: 1rem;

`;
export const Form = styled.form`
    width: 500px;
    height: 4rem;
    display: flex;
    flex-wrap:  wrap;
    margin: 0 auto;
    align-items: flex-start;
    
`;
export const Input = styled.input`
    padding: 0.5rem;
    margin-right: 1rem;
    width: 200px;
    height: 1rem;
`;
export const ButtonSearch = styled.button`
    height: 2rem;
    background-color: rgb(95, 0, 250);
    padding: 0.5rem 1rem;
    border-radius: 3px;
    font-weight: bold;
    font-size: 0.8rem;
    color: white;
    border: none;
    :active {
        background-color: rgb(95, 100, 250);
    }
`;
export const ErrorMsg = styled.div `
    color: red;
    margin-top: 0.2rem;
    height: 2rem;
    font-size: 0.8rem;
`;
    
