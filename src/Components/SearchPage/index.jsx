import './style'
import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { useState } from 'react';

import { ContainerSeachPage, Input, ButtonSearch, Form, ErrorMsg } from './style';

import Card from '../Cards';

function SearchPage () {
    const [validatedPages, setValidatedPages] = useState([])
    const [value, setVaule] = useState('')
    const [isValidPage, setIsValidPage] = useState(true)
    const [containPage, sertContaiPage] = useState(false)
 
    const formSchema = yup.object().shape({
        search: yup
        .string()
        .required("adicione uma API válida")
        .matches(
            /([a-zA-Z])([\\/])([a-zA-Z])/,
            "O caminho deve ser equivalente a: owner/repo"),
    });
    const {
        register,
        handleSubmit,
        formState: {errors}
    } = useForm({
        resolver:yupResolver(formSchema)
    })

    const validatedPagesMap = validatedPages.map((item)=> item.id)

    const printNotFound= () => {
        if (!isValidPage){
            setTimeout(() => {
            setIsValidPage(true)
        }, 1300);
        }
    }
    const validateApi = (value) => {
        if (!validatedPagesMap.includes(value.id) && value.message !== "Not Found") {
            setValidatedPages([...validatedPages, value])
        }
        if(value.message === "Not Found") {
            setIsValidPage(false)
            setTimeout(() => {
                setIsValidPage(true)
            }, 1000);
        }
        if(validatedPagesMap.includes(value.id)) {
            sertContaiPage(true)
            setTimeout(() => {
                sertContaiPage(false)
            }, 1000);
        }
    }
    const foundPage = () => {
        if (!isValidPage){
            setIsValidPage(true)
        }
     }
    const submitValue = (e) => {
        fetch(`https://api.github.com/repos/${value}`)
        .then((res) => res.json())
        .then((res) =>validateApi(res))
        .catch((err) => foundPage())        
    }

    return (
        <ContainerSeachPage>
            <Form onSubmit= {handleSubmit(submitValue)}>
                <Input
                    {...register("search")}
                    placeholder= 'owner/repo'
                    value= {value}
                    onChange = {(e) => setVaule(e.target.value)}
                    />
                <ButtonSearch type='submit'>
                    Procurar Repositório
                </ButtonSearch>
                <ErrorMsg>
                    {errors.search?.message}
                </ErrorMsg>
                <ErrorMsg>
                {
                !isValidPage &&
                    'Página não encontrada'
                }
                </ErrorMsg>
                <ErrorMsg>
                {
                containPage &&
                    'Página já adicionada'
                }
                </ErrorMsg>
            </Form>
            <div>
                <Card
                    validatedPages = {validatedPages}
                />
            </div>
        </ContainerSeachPage>
    )

}

export default SearchPage
