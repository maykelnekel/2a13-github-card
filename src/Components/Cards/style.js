import styled from "styled-components";

export const ContainerOfCards = styled.div`

`;

export const  Cards = styled.div`
    width: 400px;
    height: 120px;
    background-color: rgb(236, 244, 244);
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin: 1rem;

`;
export const CardImage = styled.img `
    width: 20%;
    height: 70%;
    margin-right: 0.5rem;
    margin-left: 0.5rem;
`;
export const ContainerInfo = styled.div `
    height: 100%;
    display: flex;
    flex-flow: column wrap;
    align-items: flex-start;
    justify-content: space-around;
    max-width: 70%;
    border-left: 2px dashed gray;
    padding-left: 1rem;


`;
export const CardTitle = styled.p `
    margin: 0;
    font-size: 1rem;
    font-weight: bold;
`;
export const CardLink = styled.a `
    height: 1rem;
    font-size: 0.7rem;
    text-align: left;
`;
export const CardDescription = styled.p `
    font-size: 0.8rem;
    margin: 0;
    text-align: left;
`;
export const CardForks = styled.p `
    margin: 0;
    font-size: 0.8rem;
    font-weight: bold;
`;
