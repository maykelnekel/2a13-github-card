import './style'
import {
    Cards,
    ContainerOfCards,
    CardImage,
    CardLink,
    CardDescription,
    CardForks,
    ContainerInfo,
    CardTitle
} from './style'

export default function Card ({validatedPages}) {
    return (
        <ContainerOfCards>
            {
                validatedPages.map((item) => 
                    item.owner !== undefined 
                   &&
                    <Cards key={item.id} >
                        <CardImage src={item.owner.avatar_url}></CardImage>
                        <ContainerInfo>
                            <CardTitle>Repositório: {item.name}</CardTitle>
                            <CardDescription>{item.description}</CardDescription>
                            <CardForks>Forks: {item.forks}</CardForks>
                            <CardLink target='_blank' href={item.html_url}>Acessar repositório no GitHub</CardLink>
                        </ContainerInfo>
                    </Cards>
                )
            }
        </ContainerOfCards>
    )
}
